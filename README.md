# podLjtTest

[![CI Status](http://img.shields.io/travis/liujingtao/podLjtTest.svg?style=flat)](https://travis-ci.org/liujingtao/podLjtTest)
[![Version](https://img.shields.io/cocoapods/v/podLjtTest.svg?style=flat)](http://cocoapods.org/pods/podLjtTest)
[![License](https://img.shields.io/cocoapods/l/podLjtTest.svg?style=flat)](http://cocoapods.org/pods/podLjtTest)
[![Platform](https://img.shields.io/cocoapods/p/podLjtTest.svg?style=flat)](http://cocoapods.org/pods/podLjtTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

podLjtTest is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'podLjtTest'
```

## Author

liujingtao, 303991250@qq.com

## License

podLjtTest is available under the MIT license. See the LICENSE file for more info.
